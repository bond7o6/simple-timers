;;; simple-timers.el --- A simple timer and stopwatch -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Arlo Hobbs
;;
;; Author: Arlo Hobbs <https://gitlab.com/bond7o6>
;; Maintainer: Arlo Hobbs <arlohobbs@hotmail.com>
;; Created: December 19, 2021
;; Modified: December 19, 2021
;; Version: 0.0.1
;; Keywords: convenience tools
;; Homepage: https://gitlab.com/bond7o6/simple-timers
;; Package-Requires: ((emacs "27.1") (alert "1.2"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Provides a simple timer and stopwatch with pause/resume functionality and
;;  modeline visibility.
;;
;;; Code:

(require 'alert)
(require 'seq)

(defgroup simple-timers nil
  "A simple timer and stopwatch."
  :group 'tools)

;; ;; TODO
;; (defcustom simple-timers-audio-player-executable (or (executable-find "afplay")
;;                                                      (executable-find "aplay"))
;;   "Music player executable.
;; Necessary for playing sounds. The program has to be able to play
;; .wav files with the invokation like <player> <file>.wav"
;;   :group 'simple-timers
;;   :type 'string)

;; ;; TODO
;; (defcustom simple-timers-audio-enabled nil
;;   "Whether to play sounds.
;; The `simple-timers-audio-player-executable' variable has to be
;; set up. `simple-timers-audio-file' stores the path to the audio
;; file to play on timer complete."
;;   :group 'simple-timers
;;   :type 'boolean)

;; ;; TODO
;; (defcustom simple-timers-audio-file ""
;;   "Path to audio file on timer complete."
;;   :group 'simple-timers
;;   :type 'string)

(defcustom simple-timers-notification-message "Timer done!"
  "Message when timer completes."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-notification-title "Emacs: simple-timers"
  "Title for notification on timer completion."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-on-tick-hook nil
  "A hook to run on every tick when the timer is running."
  :group 'simple-timers
  :type 'hook)


;;; mode-line faces
(defface simple-timers-mode-line
  '((t :inherit mode-line))
  "Parent face for simple-timers modeline text."
  :group 'simple-timers)

(defface simple-timers-mode-line-outer
  '((t :inherit simple-timers-mode-line))
  "Face for simple-timers modeline outer chars."
  :group 'simple-timers)

(defface simple-timers-mode-line-timer-outer
  '((t :inherit simple-timers-mode-line-outer))
  "Face for simple-timers modeline timer outer chars."
  :group 'simple-timers)

(defface simple-timers-mode-line-stopwatch-outer
  '((t :inherit simple-timers-mode-line-outer))
  "Face for simple-timers modeline stopwatch outer chars."
  :group 'simple-timers)

(defface simple-timers-mode-line-time
  '((t :inherit simple-timers-mode-line))
  "Face for simple-timers modeline time texts."
  :group 'simple-timers)

(defface simple-timers-mode-line-time-sep
  '((t :inherit simple-timers-mode-line-time))
  "Face for simple-timers modeline time separator text."
  :group 'simple-timers)

(defface simple-timers-mode-line-timer-time
  '((t :inherit simple-timers-mode-line-time))
  "Face for simple-timers modeline timer time text."
  :group 'simple-timers)

(defface simple-timers-mode-line-stopwatch-time
  '((t :inherit simple-timers-mode-line-time))
  "Face for simple-timers modeline stopwatch time text."
  :group 'simple-timers)

(defface simple-timers-mode-line-timer-hour
  '((t :inherit simple-timers-mode-line-timer-time))
  "Face for simple-timers modeline timer hour text."
  :group 'simple-timers)

(defface simple-timers-mode-line-timer-minute
  '((t :inherit simple-timers-mode-line-timer-time))
  "Face for simple-timers modeline timer minute text."
  :group 'simple-timers)

(defface simple-timers-mode-line-timer-second
  '((t :inherit simple-timers-mode-line-timer-time))
  "Face for simple-timers modeline timer second text."
  :group 'simple-timers)

(defface simple-timers-mode-line-timer-sep
  '((t :inherit simple-timers-mode-line-time-sep))
  "Face for simple-timers modeline timer time sep text."
  :group 'simple-timers)

(defface simple-timers-mode-line-stopwatch-hour
  '((t :inherit simple-timers-mode-line-stopwatch-time))
  "Face for simple-timers modeline stopwatch hour text."
  :group 'simple-timers)

(defface simple-timers-mode-line-stopwatch-minute
  '((t :inherit simple-timers-mode-line-stopwatch-time))
  "Face for simple-timers modeline stopwatch minute text."
  :group 'simple-timers)

(defface simple-timers-mode-line-stopwatch-second
  '((t :inherit simple-timers-mode-line-stopwatch-time))
  "Face for simple-timers modeline stopwatch second text."
  :group 'simple-timers)

(defface simple-timers-mode-line-stopwatch-sep
  '((t :inherit simple-timers-mode-line-time-sep))
  "Face for simple-timers modeline stopwatch time sep text."
  :group 'simple-timers)

;; TODO Not used
(defface simple-timers-mode-line-timer-done
  '((t :inherit simple-timers-mode-line-timer-time))
  "Face for when simple-timers timer is done."
  :group 'simple-timers)

;;; mode-line strings
(defcustom simple-timers-mode-line-format-first-char-string ""
  "First character in the simple-timers modeline string."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-mode-line-format-string "%1$s %2$s"
  "Format string for simple-timers modeline string.
`%1$s' is the timer string.
`%2$s' is the stopwatch string."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-mode-line-format-first-char-timer-string "["
  "First character in the simple-timers modeline timer string."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-mode-line-format-last-char-timer-string "]"
  "Last character in the simple-timers modeline timer string."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-mode-line-format-timer-sep-string ":"
  "Separator for simple-timers modeline timer string."
  :group 'simple-timers
  :type 'string)

;; TODO Not used
(defcustom simple-timers-mode-line-format-timer-string ""
  "Format string for simple-timers modeline timer."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-mode-line-format-first-char-stopwatch-string "("
  "First character in the simple-timers modeline stopwatch string."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-mode-line-format-last-char-stopwatch-string ")"
  "Last character in the simple-timers modeline stopwatch string."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-mode-line-format-stopwatch-sep-string ":"
  "Separator for simple-timers modeline stopwatch string."
  :group 'simple-timers
  :type 'string)

;; TODO Not used
(defcustom simple-timers-mode-line-format-stopwatch-string ""
  "Format string for simple-timers modeline stopwatch."
  :group 'simple-timers
  :type 'string)

(defcustom simple-timers-mode-line-format-last-char-string ""
  "Last character in the simple-timers modeline string."
  :group 'simple-timers
  :type 'string)

(defvar simple-timers-current-mode-line-string ""
  "Current modeline string of the timer and stopwatch.
Updated by `simple-timers-update-mode-line-string'.")


;;; clock

(defvar simple-timers--timer nil
  "A variable for the simple-timers timer.")

;; Add these two to simple-timers--state?
(defvar simple-timers-timer-sec nil
  "Current timer time in seconds.")

(defvar simple-timers-stopwatch-sec nil
  "Current stopwatch time in seconds.")

(defvar simple-timers--state nil
  "The current state of simple-timers.
This is an alist with the following keys:
- stopwatch: either 'stopped, 'paused, or 'running
- timer: either 'stopped, 'paused, or 'running")

(defun simple-timers-format-mode-line ()
  "Format a string for the modeline."
  (concat
   (propertize simple-timers-mode-line-format-first-char-string
               'face 'simple-timers-mode-line-outer)
   (format simple-timers-mode-line-format-string
           (if (or (equal (alist-get 'timer simple-timers--state) 'running)
                   (equal (alist-get 'timer simple-timers--state) 'paused))
               (format-seconds
                (concat
                 (propertize simple-timers-mode-line-format-first-char-timer-string
                             'face 'simple-timers-mode-line-timer-outer)
                 (propertize "%.2h"
                             'face 'simple-timers-mode-line-timer-hour)
                 (propertize simple-timers-mode-line-format-timer-sep-string
                             'face 'simple-timers-mode-line-timer-sep)
                 (propertize "%.2m"
                             'face 'simple-timers-mode-line-timer-minute)
                 (propertize simple-timers-mode-line-format-timer-sep-string
                             'face 'simple-timers-mode-line-timer-sep)
                 (propertize "%.2s"
                             'face 'simple-timers-mode-line-timer-second)
                 (propertize simple-timers-mode-line-format-last-char-timer-string
                             'face 'simple-timers-mode-line-timer-outer))
                simple-timers-timer-sec)
             "")
           (if (or (equal (alist-get 'stopwatch simple-timers--state) 'running)
                   (equal (alist-get 'stopwatch simple-timers--state) 'paused))
               (format-seconds
                (concat
                 (propertize simple-timers-mode-line-format-first-char-stopwatch-string
                             'face 'simple-timers-mode-line-stopwatch-outer)
                 (propertize "%.2h"
                             'face 'simple-timers-mode-line-stopwatch-hour)
                 (propertize simple-timers-mode-line-format-stopwatch-sep-string
                             'face 'simple-timers-mode-line-stopwatch-sep)
                 (propertize "%.2m"
                             'face 'simple-timers-mode-line-stopwatch-minute)
                 (propertize simple-timers-mode-line-format-stopwatch-sep-string
                             'face 'simple-timers-mode-line-stopwatch-sep)
                 (propertize "%.2s"
                             'face 'simple-timers-mode-line-stopwatch-second)
                 (propertize simple-timers-mode-line-format-last-char-stopwatch-string
                             'face 'simple-timers-mode-line-stopwatch-outer))
                simple-timers-stopwatch-sec)
             ""))
   (propertize simple-timers-mode-line-format-last-char-string
               'face 'simple-timers-mode-line-outer)))

(defun simple-timers-update-mode-line-string ()
  "Update the modeline string for the timer and stopwatch.
This sets the variable `simple-timers-current-mode-line-string'
with a value from `simple-timers-format-mode-line'.

This is made so to minimise the load on the modeline, because
otherwise the updates may be quite frequent. To add this to the
modeline, activate the `simple-timers-mode-line-mode' minor
mode."
  (setq simple-timers-current-mode-line-string (simple-timers-format-mode-line)))

(define-minor-mode simple-timers-mode-line-mode
  "Global minor mode for displaying the timer and stopwatch status in the modeline."
  :require 'simple-timers
  :global t
  :group 'simple-timers
  :after-hook
  (progn
    (if simple-timers-mode-line-mode
        (progn
          (add-to-list 'mode-line-misc-info '(:eval simple-timers-current-mode-line-string))
          (add-hook 'simple-timers-on-tick-hook #'simple-timers-update-mode-line-string)
          (add-hook 'simple-timers-on-tick-hook #'force-mode-line-update))
      (setq mode-line-misc-info (delete '(:eval simple-timers-current-mode-line-string) mode-line-misc-info))
      (remove-hook 'simple-timers-on-tick-hook #'simple-timers-update-mode-line-string)
      (remove-hook 'simple-timers-on-tick-hook #'force-mode-line-update))))

(defun simple-timers-dispatch-notification ()
  "Dispatch a notification at timer completion."
  (alert simple-timers-notification-message
         :title simple-timers-notification-title))

(defun simple-timers--on-tick ()
  "A function to be ran on a timer tick."
  (progn
    (pcase (alist-get 'timer simple-timers--state)
      ('stopped (when simple-timers--timer
                  (cancel-timer simple-timers--timer)
                  (setq simple-timers--timer nil)))
      ('paused nil)
      ('running
       (progn
         (setq simple-timers-timer-sec (1- simple-timers-timer-sec))
         ;; TODO Check if finished
         (run-hooks 'simple-timers-on-tick-hook))))
    (pcase (alist-get 'stopwatch simple-timers--state)
      ('stopped (when simple-timers--timer
                  (cancel-timer simple-timers--timer)
                  (setq simple-timers--timer nil)))
      ('paused nil)
      ('running
       (progn
         (setq simple-timers-stopwatch-sec (1+ simple-timers-stopwatch-sec))
         (run-hooks 'simple-timers-on-tick-hook))))))

;;; TODO
;;;###autoload
(defun simple-timers-timer ()
  "Start a simple-timers timer."
  (interactive))

(defun simple-timers-timer-pause ()
  "Pause the current simple-timers timer."
  (interactive))

(defun simple-timers-timer-resume ()
  "Resume the current simple-timers timer."
  (interactive))

(defun simple-timers-timer-stop ()
  "Stop the current simple-timers timer."
  (interactive))

;;;###autoload
(defun simple-timers-stopwatch ()
  "Start a simple timers stopwatch."
  (interactive)
  (setq simple-timers-stopwatch-sec 0)
  (setf (alist-get 'stopwatch simple-timers--state) 'running)
  (unless simple-timers--timer
    (setq simple-timers--timer (run-with-timer 0 1 'simple-timers--on-tick))))

(defun simple-timers-stopwatch-pause ()
  "Pause the current simple-timers stopwatch."
  (interactive)
  (setf (alist-get 'stopwatch simple-timers--state) 'paused)
  (unless (not (alist-get 'timer simple-timers--state))
    (cancel-timer simple-timers--timer)
    (setq simple-timers--timer nil)))

(defun simple-timers-stopwatch-resume ()
  "Resume the current simple-timers stopwatch."
  (interactive)
  (setf (alist-get 'stopwatch simple-timers--state) 'running)
  (unless simple-timers--timer
    (setq simple-timers--timer (run-with-timer 0 1 'simple-timers--on-tick))))

(defun simple-timers-stopwatch-stop ()
  "Stop the current simple-timers stopwatch."
  (interactive)
  (setf (alist-get 'stopwatch simple-timers--state) 'stopped)
  (unless (not (alist-get 'timer simple-timers--state))
    (cancel-timer simple-timers--timer)
    (setq simple-timers--timer nil))
  (simple-timers-update-mode-line-string))

(provide 'simple-timers)
;;; simple-timers.el ends here
